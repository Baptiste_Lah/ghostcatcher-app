﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class ParticleScript : YCBehaviour
    {
        private ParticleSystem PSystem;
        public Player player;
        // Start is called before the first frame update
        void Start()
        {
            PSystem = GetComponent<ParticleSystem>();
        }

        // Update is called once per frame
        void Update()
        {

        }
        public void OnParticleCollision(GameObject other)
        {
            if (other.tag == "Ghost"&& other.GetComponent<GhostCollider>().ghostScript.unveiled)
            {
                other.GetComponent<GhostCollider>().Dragged(true);
                //Debug.LogError("ppoiesqlihqdshbiudgkuyfezqg outfrezhr");
                player.CatchMeter(0.002f/ other.GetComponent<GhostCollider>().HP);
                player.timer3 = 0.1f;
                if (player.fillAmount >= 1)
                {
                    other.GetComponent<GhostCollider>().ghostScript.Trapped();
                    player.TrapActivation();
                }
            }
        }
    }
}

