﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class GhostCollider : MonoBehaviour
    {
        public float HP;
        public GhostScript ghostScript;
        public void Dragged(bool a)
        {
            ghostScript.Dragged(a);
        }
    }
}
