﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class TargetLauncher : YCBehaviour
    {
        public GhostScript ghostScript;

        public void GhostLaunch()
        {
            ghostScript.GoOut();
        }
    }
}
