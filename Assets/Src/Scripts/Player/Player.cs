using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System.Linq;
using DigitalRubyShared;
namespace YsoCorp {

    public class Player : YCBehaviour
    {
        public VariableJoystick variableJoystick;
        public GameObject pkMeter, protonPack;
        public Vector3 originPos,  pos, destPos, outPos, trapPos;
        private float timer1, timer2;
        public float timer3;

        public Material material;
        public MeshRenderer screen;
        public GameObject target;
        public float distance;
        public List<Transform> pivotDistance;
        public Vector3 pos1, pos2, destPos1, destPos2;
        public Camera camera;

        public bool switchMode, launched, stopMove;
        public float sensitivity;

        public GameObject protonFX;
        public Animator protonAnim;

        public float pitchSpeed, yawSpeed;

        public Rigidbody rb, selfRB;

        public Animator animator;
        public Animation shakingObject;

        public Text objective;
        public Image fillBar;
        public float fillAmount;

        public GameObject objectHolder, ghostTrapTarget, objectParent, trapObject, animationControls;

        public int ghostTrapped, ghostObjective, currentTarget;
        public List<GameObject> targetList, ghostTrapList, shakingObjectList, playerStayList;
        private bool furnitureShaking;

        public bool _panDown = false;
        private Vector3 _currentRotAngle;
        private static float MIN_PAN_DISTANCE = 0.025f;
        private static float MAX_PAN_DISTANCE = 0.1f;
        PanGestureRecognizer pan;
        private void Start()
        {
            originPos = pkMeter.transform.localPosition;
            pos = new Vector3(pkMeter.transform.localPosition.x + 0.5f, pkMeter.transform.localPosition.y - 1f, -5.5f);
            destPos = new Vector3(pos.x, pos.y - 4, pos.z);
            outPos = protonPack.transform.position;
            pos1 = pivotDistance[0].localRotation.eulerAngles;
            pos2 = pivotDistance[1].localRotation.eulerAngles;
            trapPos = trapObject.transform.localPosition;
        }
        public void InitGame()
        {
            CancelInvoke("PrepareGear");
            CancelInvoke("Win");
            CancelInvoke("GetTrapBack");
            objective.text = "";
            transform.position = Vector3.zero;
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            switchMode = false;
            launched = false;
            pkMeter.transform.position = originPos;
            protonPack.transform.position = outPos;
            SetScreenColor(0);
            fillAmount = 0;
            fillBar.fillAmount = 0;
            animator.SetTrigger("Restart");
            animator.SetBool("Open",false);
            pivotDistance[0].transform.localRotation = Quaternion.Euler(pos1);
            pivotDistance[1].transform.localRotation = Quaternion.Euler(pos2);
            trapObject.transform.parent = objectParent.transform;
            trapObject.transform.localPosition = trapPos;
            trapObject.transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
            ghostTrapped = 0;
            stopMove = false;
            protonFX.SetActive(false);
            timer1 = 0;
            timer2 = 0;
            timer3 = 0;
            animationControls.SetActive(true);
        }

        public void GameStart()
        {
            animationControls.SetActive(false);
            InitPan();
            Invoke("TargetFind", 0.2f);
        }
        public void RemoveControls()
        {
            animationControls.SetActive(false);
        }
        private void FixedUpdate()
        {
            if (this.game.state == Game.States.Playing)
            {
                if (!switchMode)
                {
                    if (!stopMove)
                    {
                        //JoystickMove();                       
                    }
                    DistanceFeedback();
                }
                else if (switchMode && launched)
                {
                    CameraControl();
                }
                if (timer1 > 0)
                {
                    pkMeter.transform.localPosition = Vector3.Lerp(pkMeter.transform.localPosition, destPos, Time.deltaTime * 10f);
                    timer1 -= Time.deltaTime;
                }
                if (timer2 > 0 && timer1 <= 0)
                {
                    protonPack.transform.localPosition = Vector3.Lerp(protonPack.transform.localPosition, pos, Time.deltaTime * 10f);
                    timer2 -= Time.deltaTime;
                }
                if (timer2 <= 0 && timer1 <= 0 && !launched && switchMode)
                {
                    Debug.LogError("osifhzoiefh");
                    protonFX.SetActive(true);
                    protonAnim.SetBool("Start", true);
                    launched = true;
                }
                if (timer3 > 0)
                {
                    timer3 -= Time.deltaTime;
                    protonPack.transform.localPosition = pos+ Random.insideUnitSphere * 3f * Time.deltaTime;
                    //protonAnim.SetBool("Start", true);
                }
                else if (timer3 <= 0)
                {
                    protonAnim.SetBool("Start", false);
                }
            }
            else
            {
                if (this.game.state!= Game.States.Win)
                {
                    if (variableJoystick.Horizontal != 0 || variableJoystick.Vertical != 0)
                    {
                        this.game.state = Game.States.Playing;
                        animationControls.SetActive(false);
                    }
                }
            }
        }
        public void Win()
        {
            this.game.state = Game.States.Win;
        }

        void JoystickMove()
        {
            if (variableJoystick.Horizontal != 0)
            {
                if (!_panDown)
                {
                    _currentRotAngle = this.transform.localEulerAngles;
                    _panDown = true;
                }
                Vector3 angle = _currentRotAngle +
                            new Vector3(0, variableJoystick.Horizontal * 100f, 0) * 2 / this.ScreenScaleH() * 0.1f;
                this.transform.localEulerAngles = angle;
            }
            //objectHolder.transform.Rotate(Vector3.up * {300f * Time.deltaTime * variableJoystick.Horizontal);
            Vector3 direction = transform.forward * variableJoystick.Vertical;
            selfRB.AddForce(direction * 300f * Time.fixedDeltaTime, ForceMode.VelocityChange);
            if (variableJoystick.Horizontal == 0)
            {
                _panDown = false;
            }
        }
        private void InitPan()
        {
            pan = new PanGestureRecognizer();
            pan.ThresholdUnits = 0f;
            pan.StateUpdated += (GestureRecognizer gesture) => {
                if (this.game.state != Game.States.Playing)
                {
                    return;
                }

                switch (pan.State)
                {
                    case GestureRecognizerState.Began:
                        this._panDown = true;
                        break;
                    case GestureRecognizerState.Executing:
                        {
                            if (this._panDown == true)
                            {
                                Vector3 angle = this.transform.localEulerAngles +
                                new Vector3(0, pan.DeltaX * 2f, 0) / this.ScreenScaleH() * 0.1f;
                                this.transform.localEulerAngles = angle;
                                var viewportDist = this.cam.ycCamera.ScreenToViewportPoint(new Vector3(0, pan.DistanceY));
                                if (Mathf.Abs(viewportDist.y) > MIN_PAN_DISTANCE)
                                {
                                    var direction = viewportDist.y >= 0 ? 1 : -1;
                                    var dist = Mathf.Lerp(0, 1, (Mathf.Abs(viewportDist.y) - MIN_PAN_DISTANCE) / MAX_PAN_DISTANCE) * direction;

                                    selfRB.velocity = this.transform.forward * dist * 25f;
                                }
                            }

                            break;
                        }
                    case GestureRecognizerState.Ended:
                        this._panDown = false;
                        break;
                }
            };
            FingersScript.Instance.AddGesture(pan);
            FingersScript.Instance.ShowTouches = false;
        }
        void CameraControl()
        {
            float pitch = 60f * Time.deltaTime * variableJoystick.Vertical;
            float yaw = 60f * Time.deltaTime * variableJoystick.Horizontal;
            transform.Rotate(0, yaw, 0, Space.World);
            //objectHolder.transform.Rotate(-pitch, 0, 0, Space.Self);

            float angle = objectHolder.transform.localEulerAngles.x;
            angle = (angle > 180) ? angle - 360 : angle;

            if (angle >-80 && pitch > 0 || angle < 80&& pitch <0)
            {
                transform.Rotate(-pitch, 0, 0, Space.Self);
            }
        }
        private void Update()
        {
            if (Input.GetKeyDown("d"))
            {
                PrepareGear();
            }
            if (Input.GetKeyDown("e"))
            {
                PrepareCatch();
            }
            if (Input.GetKeyDown("f"))
            {
                TrapActivation();
            }
        }
        void TargetFind()
        {
            //target = GameObject.Find("Target");
            targetList = GameObject.Find("LevelObjective").GetComponent<LevelObjective>().targetList;
            shakingObjectList = GameObject.Find("LevelObjective").GetComponent<LevelObjective>().shakingObjectList;
            ghostTrapList = GameObject.Find("LevelObjective").GetComponent<LevelObjective>().ghostTrapList;
            playerStayList = GameObject.Find("LevelObjective").GetComponent<LevelObjective>().playerStayList;
            ghostObjective = targetList.Count;
            ghostTrapTarget = GameObject.Find("GhostTrapTarget");
            if (ghostObjective > 1)
            {
                objective.text = "Search The Ghosts !";
            }
            else
            {
                objective.text = "Search The Ghost !";
            }

        }
        public void TrapActivation()
        {
            animator.SetBool("Open", true);
            protonFX.SetActive(false);
            protonAnim.SetBool("Start", false);
            objective.text = "Well Done !";
            ghostTrapped++;
            if (ghostTrapped >= ghostObjective)
            {
                Invoke("Win", 3f);
            }
            else
            {               
                Invoke("GetTrapBack", 3f);
            }
        }
        void PrepareGear()
        {
            timer1 = 1f;
            timer2 = 1.5f;
            switchMode = true;
            //rb.AddForce(objectHolder.transform.forward * 60000f);
            trapObject.transform.parent = null;
            //trapObject.transform.DOMove(ghostTrapTarget.transform.position, 0.5f);
            trapObject.transform.DOMove(ghostTrapList[currentTarget].transform.position, 0.5f);
            objective.text = "Capture The Ghost !";
        }
        void PrepareCatch()
        {
            //timer3 = 1f;
            //transform.DOMove(new Vector3(25, 0, 4), 0.5f);
            pan.Enabled = false;
            transform.DOMove(playerStayList[currentTarget].transform.position, 0.5f);
            transform.DORotate(playerStayList[currentTarget].transform.localEulerAngles, 0.8f);
            Invoke("PrepareGear", 2f);
        }

        void SetScreenColor(float a)
        {
            Material[] materials = screen.materials;
            Color col = Color.Lerp(Color.green, Color.red, a);
            materials[3].color = col;
            materials[3].SetColor("_EmissionColor", col);
            screen.materials = materials;
        }

        void GetTrapBack()
        {
            transform.DOLocalRotate(new Vector3(0, transform.localRotation.y, 0), 1f);
            trapObject.transform.parent = objectParent.transform;
            trapObject.transform.DOLocalMove(trapPos, 1f);
            trapObject.transform.DOLocalRotate(new Vector3(0, objectHolder.transform.localRotation.y, 0), 1f);
            protonPack.transform.DOLocalMove(outPos, 1f);
            pkMeter.transform.DOLocalMove(originPos, 1f);
            switchMode = false;
            launched = false;
            targetList.Remove(targetList[currentTarget]);
            shakingObjectList[currentTarget].GetComponent<ShakingFurniture>().Shaking(false);
            furnitureShaking = false;
            shakingObjectList.Remove(shakingObjectList[currentTarget]);
            ghostTrapList.Remove(ghostTrapList[currentTarget]);
            playerStayList.Remove(playerStayList[currentTarget]);
            SetScreenColor(0);
            fillAmount = 0;
            fillBar.fillAmount = 0;
            animator.SetTrigger("Restart");
            animator.SetBool("Open", false);
            if (ghostObjective - ghostTrapped > 1)
            {
                objective.text = (ghostObjective - ghostTrapped).ToString() + " Ghosts Remaining !";
            }
            else
            {
                objective.text = (ghostObjective - ghostTrapped).ToString() + " Ghost Remaining !";
            }
            stopMove = false;
            pan.Enabled = true;

        }

        private void DistanceFeedback()
        {
            target = nearestTarget();
            currentTarget = targetList.IndexOf(target);
            if (target != null)
            {
                distance = 1 - (Vector3.Distance(target.transform.position, transform.position)) / 50;
                SetScreenColor(distance);

                pivotDistance[0].transform.localRotation = Quaternion.Lerp(Quaternion.Euler(pos1), Quaternion.Euler(destPos1), distance);
                pivotDistance[1].transform.localRotation = Quaternion.Lerp(Quaternion.Euler(pos2), Quaternion.Euler(destPos2), distance);

                //pkMeter.transform.localPosition = Vector3.Lerp(pkMeter.transform.localPosition, originPos + Random.insideUnitSphere * distance*1.5f, Time.deltaTime * 4f);
                if (distance > 0.65f)
                {
                    pkMeter.transform.localPosition = originPos + Random.insideUnitSphere * distance /10f;
                }
                if (distance > 0.7f&&!furnitureShaking)
                {
                    shakingObjectList[currentTarget].GetComponent<ShakingFurniture>().Shaking(true);
                    furnitureShaking = true;
                }
                else if (distance <0.7f && furnitureShaking)
                {
                    shakingObjectList[currentTarget].GetComponent<ShakingFurniture>().Shaking(false);
                    furnitureShaking = false;
                }
                if (distance > 0.85f)
                {
                    target.GetComponent<TargetLauncher>().GhostLaunch();
                    PrepareCatch();
                    stopMove = true;
                }

            }
        }

        public void CatchMeter(float a)
        {
            Color col = Color.Lerp(Color.red, Color.green, fillAmount);
            fillAmount += a;
            fillBar.fillAmount = fillAmount;
            fillBar.color = col;
        }
        public GameObject nearestTarget()
        {
            if (targetList.Count > 0)
            {
            return targetList.OrderBy(element => Vector3.Distance(element.transform.position, this.transform.position)).FirstOrDefault().gameObject;
            }
            else
            {
                return null;
            }
        }
    }

}
