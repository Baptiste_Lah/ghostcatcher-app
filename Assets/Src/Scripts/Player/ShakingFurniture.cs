﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class ShakingFurniture : YCBehaviour
    {
        private List<Transform> shakeList = new List<Transform>();
        private List<Vector3> shakePositions = new List<Vector3>();
        private bool shaking;
        // Start is called before the first frame update
        void Start()
        {
            foreach(Transform trf in transform)
            {
                shakeList.Add(trf);
                shakePositions.Add(trf.localPosition);
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (shaking)
            {
                for (int i = 0; i < shakeList.Count; i++)
                {
                    shakeList[i].transform.localPosition = shakePositions[i] + Random.insideUnitSphere * 8f*Time.deltaTime;
                }
            }
        }
        public void Shaking(bool a)
        {
            shaking = a;
        }
    }
}
