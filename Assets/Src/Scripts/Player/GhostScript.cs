﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace YsoCorp
{
    public class GhostScript : YCBehaviour
    {
        public Animator animator;
        public GameObject target, player;
        public float timer, catchTimer;
        private bool startMove;
        public GameObject fxObject;
        public SkinnedMeshRenderer mesh;
        public Material mat1, mat2;
        public GameObject ghostTrap;
        public bool trapped;
        public bool unveiled;
        public float ghostSpeed;
        public List<MeshRenderer> meshRenderers;

        private void Start()
        {
            player = GameObject.Find("Player");
            ghostTrap = GameObject.Find("GhostTrap");
        }
        public void Trapped()
        {
            animator.SetBool("Trap", true);
            trapped = true;
            transform.DOScale(0, 3);
            mesh.material = mat2;
        }
        public void GoOut()
        {
            foreach(MeshRenderer m in meshRenderers)
            {
                m.enabled = true;
            }
            mesh.enabled = true;
            timer = 2f;
            startMove = true;
        }
        private void FixedUpdate()
        {
            if (timer > 0)
            {
                transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime * 2f);
                //Debug.LogError("saucisseeeeee");
                timer -= Time.deltaTime;
            }
            if (timer <= 0 && startMove)
            {
                animator.SetBool("Start", true);
                startMove = false;
                unveiled = true;
            }
            if (catchTimer > 0&&unveiled)
            {
                catchTimer -= Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, player.transform.position, Time.deltaTime * ghostSpeed);
            }
            else if (catchTimer <= 0&&unveiled)
            {
                Dragged(false);
            }
            if (trapped)
            {
                transform.position = Vector3.Lerp(transform.position, ghostTrap.transform.position, Time.deltaTime);
            }
        }
        public void Dragged(bool a)
        {
            if (a&&unveiled)
            {
                //moveForward = true;
                catchTimer = 0.2f;
                mesh.material = mat2;
                fxObject.SetActive(true);
                Debug.LogError("saucisse");
            }
            else if (!a)
            {
                //moveForward = false;
                mesh.material = mat1;
                fxObject.SetActive(false);
            }
        }
    }
}
