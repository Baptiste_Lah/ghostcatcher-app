﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace YsoCorp
{
    public class LevelObjective : YCBehaviour
    {
        public List<GameObject> targetList, ghostTrapList, shakingObjectList, playerStayList;
        public List <string> objectiveText;
    }
}
