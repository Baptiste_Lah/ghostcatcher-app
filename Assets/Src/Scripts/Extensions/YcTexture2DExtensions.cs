using UnityEngine;

public static class YcTexture2DExtensions {

    public static Texture2D Duplicate(this Texture2D that) {
        Texture2D texture = new Texture2D(that.width, that.height);
        texture.SetPixels(that.GetPixels());
        return texture;
    }

    public static void ReplaceColor(this Texture2D that, Color color, Color colorReplace) {
        Color[] pixels = that.GetPixels();
        for (int i = 0; i < pixels.Length; i++) {
            if (pixels[i] == color) {
                pixels[i] = colorReplace;
            }
        }
        that.SetPixels(pixels);
    }

    public static Texture2D DuplicateReadable(this Texture2D that) {
        RenderTexture renderTex = RenderTexture.GetTemporary(that.width, that.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
        Graphics.Blit(that, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(that.width, that.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }

}