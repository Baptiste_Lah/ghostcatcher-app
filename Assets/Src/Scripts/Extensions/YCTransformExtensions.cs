using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace YsoCorp {
    public static class YCTransformExtensions {
        
        public static Transform ResetLocalTransform(this Transform that) {
            that.localPosition = Vector3.zero;
            that.localRotation = Quaternion.identity;
            that.localScale = Vector3.one;
            return that;
        }
        
        public static Transform ResetWorldTransform(this Transform that) {
            that.position = Vector3.zero;
            that.rotation = Quaternion.identity;
            that.localScale = Vector3.one;
            return that;
        }
    }
}