using UnityEngine.UI;

namespace YsoCorp {

    public  class MenuHome : AMenu {

        public Button bPlay;
        public Button bSetting;
        public Button bRemoveAds;

        void Start() {
            this.bPlay.onClick.AddListener(() => {
                this.ycManager.adsManager.ShowInterstitial(() => {
                    this.game.state = Game.States.Playing;
                });
            });
            this.bSetting.onClick.AddListener(() => {
                this.ycManager.settingManager.Show();
            });
            this.bRemoveAds.onClick.AddListener(() => {
                this.ycManager.inAppManager.BuyProductIDAdsRemove();
            });
        }

        public void Update() {
            if (this.ycManager.adsManager.IsInterstitialOrRewardedVisible() == false) {
                this.bRemoveAds.gameObject.SetActive(this.ycManager.ycConfig.InAppRemoveAds != "" && this.ycManager.dataManager.GetAdsShow());
            }
        }

        private void LateUpdate() {
            if (this.ycManager.adsManager.IsInterstitialOrRewardedVisible() == false) {
                this.ycManager.noInternetManager.CheckInternet();
            }
        }

    }

}
