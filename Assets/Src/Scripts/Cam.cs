using UnityEngine;

namespace YsoCorp {

    public class Cam : YCBehaviour {

        public Camera ycCamera;
        public Transform target;
        private Vector3 offset;
        public Rigidbody rb;

        public Transform Obstruction;
        protected override void Awake() {
            base.Awake();
            offset = transform.position;
            Obstruction = target;
        }

        private void FixedUpdate()
        {
            transform.position = target.position + target.forward * 0.1f;
            transform.rotation = target.rotation;
        }
        private void LateUpdate()
        {
            //ViewObstructed();
        }
        void ViewObstructed()
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, target.position-transform.position, out hit, 4.5f))
            {
                if (hit.collider.gameObject.tag != "Player")
                {
                    Obstruction = hit.transform;
                    Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.ShadowsOnly;

                    if (Vector3.Distance(Obstruction.position, transform.position)>=3f && Vector3.Distance(transform.position, target.position) >= 1.5f)
                    {
                        transform.Translate(Vector3.forward * 1.5f * Time.deltaTime);
                    }
                }
                else
                {
                    Obstruction.gameObject.GetComponent<MeshRenderer>().shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
                    if(Vector3.Distance(transform.position, target.position) < 4.5f)
                    {
                        transform.Translate(Vector3.back * 1.5f * Time.deltaTime);
                    }
                }
            }
        }
    }

}
