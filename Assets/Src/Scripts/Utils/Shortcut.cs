using UnityEngine;

namespace YsoCorp {
    public class Shortcut : YCBehaviour {

#if UNITY_EDITOR
        private void Update() {
            if (Input.GetKeyDown("r")) {
                this.dataManager.DeleteAll();
                this.resourcesManager.mapLast = null;
                this.game.state = Game.States.Home;
            }
            if (Input.GetKeyDown("a")) {
                this.resourcesManager.mapLast = null;
                this.dataManager.PrevLevel();
                this.game.state = Game.States.Home;
                this.game.state = Game.States.Playing;
            }
            if (Input.GetKeyDown("z")) {
                this.resourcesManager.mapLast = null;
                this.dataManager.NextLevel();
                this.game.state = Game.States.Home;
                this.game.state = Game.States.Playing;
            }
            if (Input.GetKeyDown("w")) {
                    this.game.Win();
            }
            if (Input.GetKeyDown("l")) {
                    this.game.Lose();
            }
        }
#endif
    }
}
