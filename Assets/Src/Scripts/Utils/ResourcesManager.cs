using UnityEngine;
using System.IO;
using System;

namespace YsoCorp {

    [DefaultExecutionOrder(-1)]
    public class ResourcesManager : AResourcesManager {

        static private string PATH_MAP = "Maps/";

        public Map forceMap;
        public int ignoreFirstMapRandom = 0;

        [YcReadOnly] public int numberMaps;

        public Map mapLast { get; set; } = null;

        private Map[] _maps;

        protected override void Awake() {
            base.Awake();
            this._maps = new Map[this.numberMaps];
        }

        private void OnDrawGizmos() {
            this.numberMaps = this.GetCount(PATH_MAP, "Map*.prefab");
        }

        private int GetCount(string path, string exp) {
            int nb = 0;
            DirectoryInfo dir = new DirectoryInfo(Application.dataPath + "/Src/Resources/" + path);
            if (dir != null) {
                FileInfo[] info = dir.GetFiles(exp, SearchOption.AllDirectories);
                bool ok = false;
                do {
                    ok = false;
                    string n = exp.Replace("*", "" + nb);
                    foreach (FileInfo i in info) {
                        if (i.Name == n) {
                            nb++;
                            ok = true;
                            break;
                        }
                    }
                } while (ok == true);
                Array.Clear(info, 0, info.Length);
            }
            return nb;
        }

        private Map _GetMap(int num) {
            num %= this._maps.Length;
            if (this._maps[num] == null) {
                this._maps[num] = this.Load<Map>(PATH_MAP + "Map" + num);
            }
            return this._maps[num];
        }

        public Map GetMap() {
#if UNITY_EDITOR
            if (this.forceMap != null) {
                return this.forceMap;
            }
#endif
            if (this.mapLast != null) {
                return this.mapLast;
            }
            int level = this.dataManager.GetLevel() - 1;
            if (level < this._maps.Length) {
                this.mapLast = this._GetMap(level);
            } else {
                this.mapLast = this._GetMap(UnityEngine.Random.Range(this.ignoreFirstMapRandom, this._maps.Length));
            }
            return this.mapLast;
        }

    }

}