using UnityEngine;
using UnityEngine.Events;

namespace YsoCorp {
    public class Game : YCBehaviour {

        public enum States {
            None,
            Home,
            Playing,
            Lose,
            Win,
        }

        private States _state = States.None;
        public States state {
            get {
                return this._state;
            }
            set {
                if (this._state != value) {
                    this._state = value;
                    this.onStateChange.Invoke(value);
                    if (value == States.Home) {
                        this.HideAllMenus();
                        this.menuHome.Display();
                        this.Reset();
                        player.InitGame();
                    } else if (value == States.Playing) {
                        this.ycManager.OnGameStarted(this.dataManager.GetLevel());
                        this.HideAllMenus();
                        this.menuGame.Display();
                        player.RemoveControls();
                    }
                    else if (value == States.Lose) {
                        this.ycManager.OnGameFinished(false);
                        this.HideAllMenus();
                        this.menuLose.Display();
                    } else if (value == States.Win) {
                        this.resourcesManager.mapLast = null;
                        this.ycManager.OnGameFinished(true);
                        this.dataManager.NextLevel();
                        this.HideAllMenus();
                        this.menuWin.Display();                       
                    }
                }
            }
        }

        public class UnityEventState : UnityEvent<States> { }

        public UnityEventState onStateChange { get; set; } = new UnityEventState();

        public MenuHome menuHome;
        public MenuGame menuGame;
        public MenuLose menuLose;
        public MenuWin menuWin;

        public Map map { get; set; } = null;

        private void Start() {
            this.game.state = States.Home;
        }

        public void Reset() {
            if (this.map != null) {
                Destroy(this.map.gameObject);
            }
            this.map = Instantiate(this.resourcesManager.GetMap(), this.transform).GetComponent<Map>();
            player.GameStart();
        }

        void HideAllMenus() {
            this.menuHome.Hide();
            this.menuGame.Hide();
            this.menuLose.Hide();
            this.menuWin.Hide();
        }
        
        public void Win() {
            if (this.game.state == States.Playing) {
                this.game.state = States.Win;
            }
        }
        public void Lose() {
            if (this.game.state == States.Playing) {
                this.game.state = States.Lose;
            }
        }

    }
}
